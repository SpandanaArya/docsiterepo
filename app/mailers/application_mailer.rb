class ApplicationMailer < ActionMailer::Base
  default from: 'Docsite@example.com'
  layout 'mailer'
end
