class DocpatientMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.docpatient_mailer.editmailer.subject
  #
  def editmailer(patient, mailtext, sub)
    @patient = patient
    @mailtext = mailtext
    @sub = sub
    mail(to: @patient.email, subject: @sub)
  end
end
