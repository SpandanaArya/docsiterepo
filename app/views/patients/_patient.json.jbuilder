json.extract! patient, :id, :name, :age, :details, :email, :appointment, :user_id, :created_at, :updated_at
json.url patient_url(patient, format: :json)
