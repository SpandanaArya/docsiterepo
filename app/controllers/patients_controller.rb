class PatientsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_patient, only: [:show, :edit, :update, :destroy]

  # GET /patients
  # GET /patients.json
  def index
    if current_user.admin
      @patients = Patient.all
    else
      @patients = Patient.where(user_id: current_user.id)
    end
    @patient = Patient.new
  end

  # GET /patients/1
  # GET /patients/1.json
  def show
  end

  # GET /patients/new
  def new
    @patient = Patient.new
  end

  # GET /patients/1/edit
  def edit
  end

  # POST /patients
  # POST /patients.json
  def create
    @patient = Patient.new(patient_params)
    @patient.user = current_user
    respond_to do |format|
      if @patient.save
        format.html { redirect_to patients_url, notice: 'Patient was successfully created.' }
        format.json { render :show, status: :created, location: @patient }
        DocpatientMailer.editmailer(@patient, 'You have been registered to DocSite', 'Welcome to Docsite!').deliver
      else
        format.html { render :new }
        format.json { render json: @patient.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /patients/1
  # PATCH/PUT /patients/1.json
  def update
    respond_to do |format|
      if @patient.update(patient_params)
        DocpatientMailer.editmailer(@patient, 'Your details with the DocSite are updated.', 'DocSite - Patient Details updated').deliver
        format.html { redirect_to patients_url, notice: 'Patient was successfully updated.' }
        format.json { render :show, status: :ok, location: @patient }
      else
        format.html { render :edit }
        format.json { render json: @patient.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /patients/1
  # DELETE /patients/1.json
  def destroy
    @patient.destroy
    DocpatientMailer.editmailer(@patient, 'Your details have been removed from DocSite', 'Docsite - Patient details removed').deliver
    respond_to do |format|
      format.html { redirect_to patients_url, notice: 'Patient was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_patient
      @patient = Patient.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def patient_params
      params.require(:patient).permit(:name, :age, :details, :email, :appointment)
    end
end
