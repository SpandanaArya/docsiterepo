class Patient < ApplicationRecord
  validates :age, inclusion: 1...60
  validates :name, length: { minimum:2 }
  validates :email, format: { with: /.*@.*/ }
  validates :details, presence: true
  belongs_to :user
end
