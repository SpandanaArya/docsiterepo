class User < ApplicationRecord
  has_many :patients, dependent: :destroy
  has_many :meetings, dependent: :destroy
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :confirmable

   def username
     return self.email.split('@')[0].capitalize
   end
end
