require 'test_helper'

class DocpatientMailerTest < ActionMailer::TestCase
  test "editmailer" do
    mail = DocpatientMailer.editmailer
    assert_equal "Editmailer", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
