# Preview all emails at http://localhost:3000/rails/mailers/docpatient_mailer
class DocpatientMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/docpatient_mailer/editmailer
  def editmailer
    DocpatientMailer.editmailer
  end

end
