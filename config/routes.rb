Rails.application.routes.draw do
  resources :meetings
  resources :patients
  resources :dashboard, only: [:index]
  devise_for :users, controllers: {
    confirmations: 'confirmations'
  }
  root 'patients#index'
  resources :home, only: [:index]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
